import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:cached_network_image/cached_network_image.dart';

class LatNetworkImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var title = 'Web Images';
    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Image.network('https://www.gambaranimasi.org/data/media/276/animasi-bergerak-sekolah-0009.gif'),
      ),
    );
  }
}

class FadeImageLat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Fade in images';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Stack(
          children: <Widget>[
            Center(child: CircularProgressIndicator()),
            Center(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: 'https://bk.penabur.sch.id/4dmin-login/assets/img/artikel/student/4peibcv0.jpg',
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LoadingImageGif extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Fade in images';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          child: FadeInImage.assetNetwork(
            placeholder: 'assets/images/loading.gif',
            image: 'https://bk.penabur.sch.id/4dmin-login/assets/img/artikel/student/4peibcv0.jpg',
          ),
        ),
      ),
    );
  }
}

class MyCachedNetworkImage extends StatelessWidget {
  const MyCachedNetworkImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const title = 'Cached Images';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: Center(
          child: CachedNetworkImage(
            placeholder: (context, url) => const CircularProgressIndicator(),
            imageUrl: 'https://bk.penabur.sch.id/4dmin-login/assets/img/artikel/student/4peibcv0.jpg',
          ),
        ),
      ),
    );
  }
}
