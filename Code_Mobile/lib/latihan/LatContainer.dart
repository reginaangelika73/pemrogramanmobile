import 'package:flutter/material.dart';

class LatContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        backgroundColor: Colors.amberAccent,
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Welcome to Flutter'),
          backgroundColor: Colors.red,
          foregroundColor: Colors.brown,
          shadowColor: Colors.black,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/strawberry.png'),
          Text(
            'Belajar Flutter untuk Pemula',
            style: TextStyle(fontSize: 24, fontFamily: "Serif", height: 2.0),
          ),
          Text('Pradita university'),
          Container(
              padding: const EdgeInsets.all(10.0),
              margin: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                border: Border.all(width: 10, color: Colors.black38),
                borderRadius: const BorderRadius.all(Radius.circular(8)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[Image.asset('assets/images/strawberry.png'), Image.asset('assets/images/pano1.png')],
              ))
        ]),
      ),
    );
  }
}
