import 'package:flutter/material.dart';

class LatStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Stack(
          children: [
            const CircleAvatar(
              backgroundImage: AssetImage('assets/images/pano1.png'),
              radius: 100,
            ),
            Container(
              width: 200,
              height: 200,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: Colors.black54,
              ),
              child: const Text(
                'AA',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
