import 'package:flutter/material.dart';

class LatPageView extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.blueGrey[900],
            title: Center(
              child: Text(
                'Flutter GridView Demo',
              ),
            ),
          ),
          body: MyStatelessWidget()),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  const MyStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);
    return PageView(
      /// [PageView.scrollDirection] defaults to [Axis.horizontal].
      /// Use [Axis.vertical] to scroll vertically.
      scrollDirection: Axis.horizontal,
      controller: controller,
      children: const <Widget>[
        Center(
          child: Text('First Page'),
        ),
        Center(
          child: Text('Second Page'),
        ),
        Center(
          child: Text('Third Page'),
        )
      ],
    );
  }
}

class ListViewIcon extends StatelessWidget {
  const ListViewIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const title = 'Basic List';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(title),
        ),
        body: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            Container(
              width: 260.0,
              color: Colors.red,
              child: ListTile(
                leading: Icon(Icons.map),
                title: Text('Map'),
              ),
            ),
            Container(
              width: 460.0,
              color: Colors.yellow,
              child: ListTile(
                leading: Icon(Icons.map),
                title: Text('Map'),
              ),
            ),
            Container(
              width: 160.0,
              color: Colors.green,
              child: ListTile(
                leading: Icon(Icons.map),
                title: Text('Map'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
