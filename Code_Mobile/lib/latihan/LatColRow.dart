import 'package:flutter/material.dart';

class LatColRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        backgroundColor: Colors.amberAccent,
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Welcome to Flutter'),
          backgroundColor: Colors.red,
          foregroundColor: Colors.brown,
          shadowColor: Colors.black,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/strawberry.png'),
          Text(
            'Belajar Flutter untuk Pemula',
            style: TextStyle(fontSize: 24, fontFamily: "Serif", height: 2.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset('assets/images/strawberry.png'),
              Image.asset('assets/images/pano1.png')
            ],
          ),
          Text('Pradita university')
        ]),
      ),
    );
  }
}
