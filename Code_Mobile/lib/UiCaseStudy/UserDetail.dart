import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'User.dart';

class UserDetail extends StatelessWidget {
  const UserDetail({Key? key, required this.user}) : super(key: key);
  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Detail'),
      ),
      body: MyTable(user: user),
    );
  }
}

/// This is the main application widget.
class MyTable extends StatelessWidget {
  const MyTable({Key? key, required this.user}) : super(key: key);
  final User user;

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(),
      columnWidths: const <int, TableColumnWidth>{
        0: FixedColumnWidth(50),
        1: FixedColumnWidth(150),
        2: FlexColumnWidth(),
      },
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(
          children: [
            Container(
              height: 64,
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              color: Colors.lightBlueAccent,
              child: Text("1",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                  )),
            ),
            Container(
              height: 64,
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              color: Colors.lightBlueAccent,
              child: Text("User ID",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                  )),
            ),
            Container(
              height: 64,
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              color: Colors.lightBlueAccent,
              child: Text(user.userId,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                  )),
            ),
          ],
        ),
        TableRow(
          children: [
            Container(
              height: 64,
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              color: Colors.lightBlueAccent,
              child: Text("2",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                  )),
            ),
            Container(
              height: 64,
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              color: Colors.lightBlueAccent,
              child: Text("Password",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                  )),
            ),
            Container(
              height: 64,
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              color: Colors.lightBlueAccent,
              child: Text(user.password,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                  )),
            ),
          ],
        ),
      ],
    );
  }
}
