import 'dart:async';
import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:flutter_application_1/serialization/address.dart';
import 'package:flutter_application_1/serialization/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:json_annotation/json_annotation.dart';

// ignore: non_constant_identifier_names
void RunAdvanceJson() async {
  var addr = new Address(street: "Jalan ccc", city: "aaa");
  var mhsObj = new User(id: 1, name: "hans", address: addr);
  String jsonString = jsonEncode(mhsObj);
  Map<String, dynamic> userMap = jsonDecode(jsonString);
  var mhs = User.fromJson(userMap);
  print('Howdy, ${mhs.name}!');
  print('Umur ${mhs.age}.');
}
