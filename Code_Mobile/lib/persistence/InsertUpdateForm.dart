import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'LatDbSqlite.dart';
import 'Mahasiswa.dart';
import 'StatefulWrapper.dart';

class InsertUpdateForm extends StatefulWidget {
  InsertUpdateForm({
    Key? key,
    required this.mahasiswa,
    required this.isUpdate,
    required this.updateList,
  }) : super(key: key);
  Mahasiswa mahasiswa;
  bool isUpdate;
  var updateList;
  var txtName = TextEditingController();

  @override
  _MahasiswaEditFormState createState() => _MahasiswaEditFormState();
}

class _MahasiswaEditFormState extends State<InsertUpdateForm> {
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    widget.txtName.text = widget.mahasiswa.name;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: widget.txtName,
                    onChanged: (value) {
                      widget.mahasiswa.name = value;
                    },
                    decoration: new InputDecoration(
                      hintText: "contoh: Haryono",
                      labelText: "Nama",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onChanged: (value) {
                      widget.mahasiswa.age = value as int;
                    },
                    decoration: new InputDecoration(
                      hintText: "contoh: Umur",
                      labelText: "Umur",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    widget.isUpdate ? "Update" : "Create",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (widget.isUpdate) {
                      var db = DbService.initDb();
                      var dbService = new DbService(database: db);
                      dbService.updateMahasiswa(widget.mahasiswa);
                    } else {
                      var db = DbService.initDb();
                      var dbService = new DbService(database: db);
                      dbService.insertMahasiswa(widget.mahasiswa);
                    }
                    widget.updateList();
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
