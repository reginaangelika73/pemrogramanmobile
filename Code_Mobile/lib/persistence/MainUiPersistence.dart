import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'InsertUpdateForm.dart';
import 'LatDbSqlite.dart';
import 'ListMhs.dart';
import 'Mahasiswa.dart';
import 'StatefulWrapper.dart';

class MainUiPersistence extends StatefulWidget {
  const MainUiPersistence({Key? key}) : super(key: key);

  @override
  _MainUiPersistence createState() => _MainUiPersistence();
}

class _MainUiPersistence extends State<MainUiPersistence> {
  Mahasiswa mahasiswa = new Mahasiswa(id: Uuid().v1(), name: "Andi", age: 2);
  late List<Mahasiswa> mahasiswas = [];

  Future<void> updateList() async {
    var db = DbService.initDb();
    var dbService = new DbService(database: db);
    var mhss = await dbService.mahasiswas();
    setState(() {
      mahasiswas = mhss;
    });
    setState(() {
      mahasiswa = new Mahasiswa(id: Uuid().v1(), name: "New", age: 2);
    });
  }

  void showMhs(Mahasiswa mhs) {
    setState(() {
      mahasiswa = mhs;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StatefulWrapper(
      child: MaterialApp(
        title: 'Flutter Demo',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Form'),
          ),
          body: Column(
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(10.0),
                  margin: const EdgeInsets.all(20.0),
                  height: 250,
                  child: InsertUpdateForm(
                    mahasiswa: mahasiswa,
                    isUpdate: false,
                    updateList: updateList,
                  )),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  margin: const EdgeInsets.all(20.0),
                  height: 250,
                  child: ListMhs(mahasiswas: mahasiswas, showMhs: showMhs))
            ],
          ),
        ),
      ),
      onInit: () {
        RunLatDB(); // run DB
      },
    );
  }
}
