import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
import 'InsertUpdateForm.dart';
import 'LatDbSqlite.dart';
import 'Mahasiswa.dart';
import 'StatefulWrapper.dart';

/// This is the stateful widget that the main application instantiates.
class ListMhs extends StatefulWidget {
  ListMhs({Key? key, required this.mahasiswas, required this.showMhs}) : super(key: key);
  List<Mahasiswa> mahasiswas;
  var showMhs;
  @override
  State<ListMhs> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<ListMhs> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.mahasiswas.length,
      itemBuilder: (BuildContext context, int index) {
        Mahasiswa mhs = widget.mahasiswas[index];
        return ListTile(
          title: Text('Name:' + mhs.name + ', Umur:' + mhs.age.toString()),
          selected: index == _selectedIndex,
          onTap: () {
            widget.showMhs(mhs);
            setState(() {
              _selectedIndex = index;
            });
          },
        );
      },
    );
  }
}
