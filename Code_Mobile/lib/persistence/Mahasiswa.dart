class Mahasiswa {
  String id;
  String name;
  int age;

  Mahasiswa({
    required this.id,
    required this.name,
    required this.age,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }

  @override
  String toString() {
    return 'Mahasiswa{id: $id, name: $name, age: $age}';
  }
}
