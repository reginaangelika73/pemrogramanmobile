import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/UiCaseStudy/LoginForm.dart';
import 'package:flutter_application_1/persistence/LatDbSqlite.dart';
import 'package:flutter_application_1/persistence/MainUiPersistence.dart';
import 'package:flutter_application_1/persistenceUI/MahasiswaViewPersistenceUI.dart';
import 'package:flutter_application_1/serialization/LatUsingLibrary.dart';
import 'package:flutter_application_1/serialization/simple.dart';

import 'IoT/MahasiswaView.dart';
import 'UiCaseStudy/MainUiCase.dart';
import 'UiCaseStudy/UserDetail.dart';
import 'googlemaps/LatMapType.dart';
import 'googlemaps/LatMarker.dart';
import 'googlemaps/LatPolyline.dart';
import 'googlemaps/latgooglemap.dart';
import 'latihan/AlertBasic.dart';
import 'latihan/AlertConfirmation.dart';
import 'latihan/AlertOption.dart';
import 'latihan/AlertTextField.dart';
import 'latihan/Forms/ComboboxForm.dart';
import 'latihan/Forms/FocusForm.dart';
import 'latihan/Forms/Form1.dart';
import 'latihan/Forms/TextEditingController.dart';
import 'latihan/Images/LatNetworkImage.dart';
import 'latihan/Lat1.dart';
import 'latihan/LatColRow.dart';
import 'latihan/LatContainer.dart';
import 'latihan/LatGridViewCount.dart';
import 'latihan/LatGridViewextent.dart';
import 'latihan/LatListView.dart';
import 'latihan/LatListViewNext.dart';
import 'latihan/LatListViewNextSelected.dart';
import 'latihan/LatPageView.dart';
import 'latihan/LatStack.dart';
import 'latihan/LatStatefulWidget.dart';
import 'latihan/LatTable.dart';
import 'latihan/ManageByBoth.dart';
import 'latihan/ManageByParent.dart';
import 'latihan/ManageOwnState.dart';
import 'latihan/PageRoutes.dart';
import 'latihan/navigations/LatAnimationScreen.dart';
import 'latihan/navigations/LatModalRouteOf.dart';
import 'latihan/navigations/LatNavigateWithRouteName.dart';
import 'latihan/navigations/LatPassListDetail.dart';
import 'latihan/navigations/LatReturnData.dart';
import 'latihan/pluggins/camera.dart';
import 'latihan/pluggins/video.dart';

void main() {
  runApp(LatListViewNext());
  //RunLatDB(); // run DB
  //RunAdvanceJson();
}

// Future<void> main() async {
//   // Ensure that plugin services are initialized so that `availableCameras()`
//   // can be called before `runApp()`
//   WidgetsFlutterBinding.ensureInitialized();

//   // Obtain a list of the available cameras on the device.
//   final cameras = await availableCameras();

//   // Get a specific camera from the list of available cameras.
//   final firstCamera = cameras.first;

//   runApp(
//     MaterialApp(
//       theme: ThemeData.dark(),
//       home: TakePictureScreen(
//         // Pass the appropriate camera to the TakePictureScreen widget.
//         camera: firstCamera,
//       ),
//     ),
//   );
// }
