import 'dart:async';
import 'package:flutter/material.dart';
import 'HttpClientService.dart';
import 'Mahasiswa.dart';

class MahasiswaView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Alert ...'),
        ),
        body: Center(
          child: PostToServerCom(),
        ),
      ),
    );
  }
}

class PostToServerCom extends StatefulWidget {
  const PostToServerCom({Key? key}) : super(key: key);

  @override
  _MyAppState createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<PostToServerCom> {
  Future<Mahasiswa>? _mhs;
  List<Mahasiswa>? _mahasiswas;
  var currentMhsName = TextEditingController();

  @override
  initState() {
    getMahasiswas();
    super.initState();
  }

  @override
  void dispose() {
    currentMhsName.dispose();
    super.dispose();
  }

  getMahasiswas() async {
    List<Mahasiswa> mahasiswas = await HttpClientService.getStudents();
    setState(() {
      _mahasiswas = mahasiswas;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Create Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Create Data Example'),
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child:
                    _mahasiswas == null ? Text('Loading...') : showMahasiwas(),
              ),
              _mhs == null ? Text('Loading...') : showMahasiwa()
            ],
          )),
    );
  }

  FutureBuilder<Mahasiswa> showMahasiwa() {
    return FutureBuilder<Mahasiswa>(
      future: _mhs,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
            height: 50,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: currentMhsName,
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    var tile = _mahasiswas!
                        .firstWhere((item) => item.Id == snapshot.data!.Id);
                    setState(() => tile.Nama = currentMhsName.text);

                    HttpClientService.updateStudent(tile);
                    setState(() {
                      _mahasiswas = _mahasiswas;
                    });
                  },
                  child: Text("Update"),
                ),
                ElevatedButton(
                  onPressed: () {
                    HttpClientService.delStudent(snapshot.data!.Id);
                    _mahasiswas!
                        .removeWhere((item) => item.Id == snapshot.data!.Id);
                    setState(() {
                      _mhs = null;
                    });
                  },
                  child: Text("X"),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        return const CircularProgressIndicator();
      },
    );
  }

  ListView showMahasiwas() {
    final List<int> colorCodes = <int>[600, 500, 100];

    var viewsMhss = ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: _mahasiswas?.length,
        itemBuilder: (BuildContext context, int index) {
          var mhs = _mahasiswas![index];
          return Container(
            margin: const EdgeInsets.all(2.0),
            height: 50,
            child: ElevatedButton(
              onPressed: () {
                currentMhsName.text = mhs.Nama;
                setState(() {
                  _mhs = HttpClientService.getStudent(mhs.Id);
                });
              },
              child: Text('${mhs.Nama}'),
            ),
          );
        });

    return viewsMhss;
  }
}
