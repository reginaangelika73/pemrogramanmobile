import 'package:flutter/material.dart';
import 'package:flutter_application_1/IoT/Mahasiswa.dart';

class LatListViewNext extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.blueGrey[900],
            title: Center(
              child: Text(
                'Flutter GridView Demo',
              ),
            ),
          ),
          body: MyList3()),
    );
  }
}

class MyList1 extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        Container(
          height: 50,
          color: Colors.lightBlueAccent[200],
          child: const Center(child: Text('Entry A')),
        ),
        Container(
          height: 50,
          color: Colors.lightBlueAccent[200],
          child: const Center(child: Text('Entry B')),
        ),
        Container(
          height: 50,
          color: Colors.lightBlueAccent[100],
          child: const Center(child: Text('Entry C')),
        ),
      ],
    );
  }
}

class MyList2 extends StatelessWidget {
  final List<String> entries = <String>['CCC', 'BBBB', 'GGG'];
  final List<int> colorCodes = <int>[600, 500, 100];

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: entries.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 50,
            color: Colors.amber[colorCodes[index]],
            child: Center(child: Text('TEST ${entries[index]}')),
          );
        });
  }
}

class MyList3 extends StatelessWidget {
  Mahasiswa mahasiswa1 = new Mahasiswa(Id: 1, Nama: "Regina", Age: "21");
  Mahasiswa mahasiswa2 = new Mahasiswa(Id: 2, Nama: "Angelika", Age: "20");
  Mahasiswa mahasiswa3 = new Mahasiswa(Id: 3, Nama: "Ayu", Age: "19");
  List<Mahasiswa> mahasiswas = [];

  initData() {
    mahasiswas = <Mahasiswa>[mahasiswa1, mahasiswa2, mahasiswa3];
  }

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(20),
      itemCount: entries.length,
      itemBuilder: (BuildContext context, int index) {
        Mahasiswa mhs = mahasiswas[index];
        return Container(
          height: 150,
          color: Colors.amber[colorCodes[index]],
          child: Center(child: Text('Nama: ${mhs.Nama}, Age: ${mhs.Age}')),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}
