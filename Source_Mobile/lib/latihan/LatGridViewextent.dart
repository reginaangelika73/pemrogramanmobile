import 'package:flutter/material.dart';

class LatGridViewextent extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Center(
            child: Text(
              'Flutter GridView Demo',
              style: TextStyle(
                color: Colors.blueAccent,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
          ),
        ),
        body: GridView.extent(
          maxCrossAxisExtent: 130,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
          shrinkWrap: true,
          children: List.generate(
            20,
            (index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 10, color: Colors.black38),
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                ),
                child: Text('Colom $index'),
              );
            },
          ),
        ),
      ),
    );
  }
}
