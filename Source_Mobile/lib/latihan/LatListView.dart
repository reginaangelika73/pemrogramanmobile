import 'package:flutter/material.dart';

class LatListView extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Center(
            child: Text(
              'Flutter GridView Demo',
              style: TextStyle(
                color: Colors.blueAccent,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
          ),
        ),
        body: ListView(
          children: List.generate(
            30,
            (index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 10, color: Colors.black38),
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                ),
                child: Text('View $index'),
              );
            },
          ),
        ),
      ),
    );
  }
}
