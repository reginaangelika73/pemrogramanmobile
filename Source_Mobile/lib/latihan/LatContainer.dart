import 'package:flutter/material.dart';

class LatContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          backwardsCompatibility: false,
          title: Text('Welcome to Flutter'),
          backgroundColor: Colors.lightBlue,
          foregroundColor: Colors.lightBlueAccent,
          shadowColor: Colors.black38,
        ),
        body: Column(children: <Widget>[
          Image.asset('assets/images/flamingo.png'),
          Text(
            'Belajar Flutter untuk Pemula',
            style: TextStyle(fontSize: 21, fontFamily: "Serif", height: 2.0),
          ),
          Text('Pradita university'),
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 10, color: Colors.black38),
                borderRadius: const BorderRadius.all(Radius.circular(8)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Image.asset('assets/images/pano32.png'),
                  Image.asset('assets/images/pano1.png')
                ],
              ))
        ]),
      ),
    );
  }
}
