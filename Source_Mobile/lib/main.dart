import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/UiCaseStudy/LoginForm.dart';
import 'package:flutter_application_1/latihan/LatColRow.dart';
import 'package:flutter_application_1/latihan/LatContainer.dart';
import 'package:flutter_application_1/latihan/LatGridViewCount.dart';
import 'package:flutter_application_1/latihan/LatGridViewextent.dart';
import 'package:flutter_application_1/latihan/LatListView.dart';

import 'latihan/AlertBasic.dart';
import 'latihan/AlertConfirmation.dart';
import 'latihan/AlertOption.dart';
import 'latihan/AlertTextField.dart';
import 'latihan/Forms/ComboboxForm.dart';
import 'latihan/Forms/FocusForm.dart';
import 'latihan/Forms/Form1.dart';
import 'latihan/Forms/TextEditingController.dart';
import 'latihan/Images/LatNetworkImage.dart';
import 'latihan/Lat1.dart';
import 'latihan/LatListViewNext.dart';
import 'latihan/LatStack.dart';
import 'latihan/LatStatefulWidget.dart';
import 'latihan/ManageByBoth.dart';
import 'latihan/ManageByParent.dart';
import 'latihan/ManageOwnState.dart';
import 'latihan/PageRoutes.dart';

void main() {
  runApp(LatListViewNext());
}
