import 'dart:async';
import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

// ignore: non_constant_identifier_names
void RunSimpleJson() async {
  var mhsObj = new Mahasiswa(id: 1, name: "hans", age: 22);
  String jsonString = jsonEncode(mhsObj);
  Map<String, dynamic> userMap = jsonDecode(jsonString);
  var mhs = Mahasiswa.fromJson(userMap);
  print('Howdy, ${mhs.name}!');
  print('Umur ${mhs.age}.');
}

class Mahasiswa {
  final int id;
  final String name;
  final int age;

  Mahasiswa({
    required this.id,
    required this.name,
    required this.age,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'age': age,
    };
  }

  Mahasiswa.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        age = json['age'];
}
