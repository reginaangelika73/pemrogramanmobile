import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';

import 'Mahasiswa.dart';

// ignore: non_constant_identifier_names
void RunLatDB() async {
  WidgetsFlutterBinding.ensureInitialized();
  final dbPath = await getDatabasesPath();
  final database = openDatabase(
    join(dbPath, 'mahasiswa_database.db'),
    onCreate: (db, version) {
      return db.execute(
        'CREATE TABLE mahasiswas(id TEXT PRIMARY KEY, name TEXT, age INTEGER)',
      );
    },
    version: 1,
  );
}

class DbService {
  final database;

  DbService({required this.database});

  static Future<Database> initDb() async {
    WidgetsFlutterBinding.ensureInitialized();
    final dbPath = await getDatabasesPath();
    final database = openDatabase(
      join(dbPath, 'mahasiswa_database.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE mahasiswas(id TEXT PRIMARY KEY, name TEXT, age INTEGER)',
        );
      },
      version: 1,
    );
    return database;
  }

  Future<void> insertMahasiswa(Mahasiswa mahasiswa) async {
    final db = await database;
    await db.insert(
      'mahasiswas',
      mahasiswa.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Mahasiswa>> mahasiswas() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('mahasiswas');
    return List.generate(maps.length, (i) {
      return Mahasiswa(
        id: maps[i]['id'],
        name: maps[i]['name'],
        age: maps[i]['age'],
      );
    });
  }

  Future<Mahasiswa> getMahasiswa(String id) async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query(
      'mahasiswas',
      where: 'id = ?',
      whereArgs: [id],
    );
    var mhss = List.generate(maps.length, (i) {
      return Mahasiswa(
        id: maps[i]['id'],
        name: maps[i]['name'],
        age: maps[i]['age'],
      );
    });

    return mhss.first;
  }

  Future<void> updateMahasiswa(Mahasiswa mahasiswa) async {
    final db = await database;
    await db.update(
      'mahasiswas',
      mahasiswa.toMap(),
      where: 'id = ?',
      whereArgs: [mahasiswa.id],
    );
  }

  Future<void> deleteMahasiswa(String id) async {
    final db = await database;
    await db.delete(
      'mahasiswas',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
