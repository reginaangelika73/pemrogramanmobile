import 'dart:async';
import 'package:flutter/material.dart';
import 'LatDbSqlite.dart';
import 'Mahasiswa.dart';
import 'package:uuid/uuid.dart';

class MahasiswaViewPersistenceUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('UI Persistence'),
        ),
        body: Center(
          child: PostToServerCom(),
        ),
      ),
    );
  }
}

class PostToServerCom extends StatefulWidget {
  const PostToServerCom({Key? key}) : super(key: key);

  @override
  _MyAppState createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<PostToServerCom> {
  DbService? dbService;

  Future<Mahasiswa>? _mhs;
  List<Mahasiswa>? _mahasiswas;
  var currentMhsName = TextEditingController();
  var currentMhsUmur = TextEditingController();
  bool showMhs = false;

  @override
  initState() {
    super.initState();

    var db = DbService.initDb();
    dbService = new DbService(database: db);
    getMahasiswas();
  }

  @override
  void dispose() {
    currentMhsName.dispose();
    super.dispose();
  }

  getMahasiswas() async {
    List<Mahasiswa> mahasiswas = await dbService!.mahasiswas();
    setState(() {
      _mahasiswas = mahasiswas;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Create Data Example',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: showMhs ? showMahasiwa() : showMahasiwas());
  }

  FutureBuilder<Mahasiswa> showMahasiwa() {
    return FutureBuilder<Mahasiswa>(
      future: _mhs,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: currentMhsName,
                  ),
                ),
                SizedBox(width: 20),
                Expanded(
                  child: TextField(
                    controller: currentMhsUmur,
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    var mhs = _mahasiswas!.firstWhere((item) => item.id == snapshot.data!.id);
                    setState(() => mhs.name = currentMhsName.text);
                    setState(() => mhs.age = int.parse(currentMhsUmur.text));
                    dbService!.updateMahasiswa(mhs);
                    setState(() {
                      _mahasiswas = _mahasiswas;

                      showMhs = false;
                    });
                  },
                  child: Text("Update"),
                ),
                ElevatedButton(
                  onPressed: () {
                    var uuid = Uuid();
                    var id = uuid.v4();
                    var mhsNew = new Mahasiswa(id: id.toString(), name: currentMhsName.text, age: int.parse(currentMhsUmur.text));
                    dbService!.insertMahasiswa(mhsNew);
                    getMahasiswas();
                    setState(() {
                      showMhs = false;
                      showMhs = false;
                    });
                  },
                  child: Text("Add"),
                ),
                ElevatedButton(
                  onPressed: () {
                    dbService!.deleteMahasiswa(snapshot.data!.id);
                    _mahasiswas!.removeWhere((item) => item.id == snapshot.data!.id);
                    setState(() {
                      _mhs = null;
                      showMhs = false;
                    });
                  },
                  child: Text("X"),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        return const CircularProgressIndicator();
      },
    );
  }

  ListView showMahasiwas() {
    var viewsMhss = ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: _mahasiswas?.length,
        itemBuilder: (BuildContext context, int index) {
          var mhs = _mahasiswas![index];
          var mhsInfo = "Nama:" + mhs.name + ", Umur:" + mhs.age.toString();
          return Container(
            margin: const EdgeInsets.all(2.0),
            height: 50,
            child: ElevatedButton(
              onPressed: () {
                currentMhsName.text = mhs.name;
                currentMhsUmur.text = mhs.age.toString();
                var mhsDB = dbService!.getMahasiswa(mhs.id);
                setState(() {
                  _mhs = mhsDB;
                });
                setState(() {
                  showMhs = true;
                });
              },
              child: Text(mhsInfo),
            ),
          );
        });

    return viewsMhss;
  }
}
