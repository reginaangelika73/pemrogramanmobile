import 'package:flutter/material.dart';

import 'LoginForm.dart';
import 'User.dart';

class MainUiCase extends StatelessWidget {
  const MainUiCase({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      home: Scaffold(
        body: Center(
          child: LoginForm(),
        ),
      ),
    );
  }
}
