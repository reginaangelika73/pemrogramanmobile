import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LatPolylineHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LatPolyline(),
    );
  }
}

class LatPolyline extends StatefulWidget {
  @override
  _LatPolylineState createState() => _LatPolylineState();
}

class _LatPolylineState extends State<LatPolyline> {
  final Set<Marker> _markers = {};
  final LatLng _currentPosition = LatLng(-7.615697, 110.939672);
  LatLng _lastPosition = LatLng(-7.615697, 110.939672);
  Set<Polyline> _polyline = {};

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Maps Flutter'),
      ),
      body: Stack(children: <Widget>[
        GoogleMap(
          polylines: _polyline,
          onTap: (position) {
            setState(() {
              _markers.add(
                Marker(
                  markerId: MarkerId("${position.latitude}, ${position.longitude}"),
                  icon: BitmapDescriptor.defaultMarker,
                  position: position,
                ),
              );
            });
            var points = [_lastPosition, position];
            setState(() {
              _polyline.add(Polyline(
                polylineId: PolylineId("${position.latitude}, ${position.longitude}"),
                visible: true,
                //latlng is List<LatLng>
                points: points,
                color: Colors.blue,
              ));
            });
          },
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: _currentPosition,
            zoom: 14.0,
          ),
          markers: _markers,
        ),
      ]),
    );
  }
}
