import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LatMarkerHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LatMarker(),
    );
  }
}

class LatMarker extends StatefulWidget {
  @override
  _LatMarkerState createState() => _LatMarkerState();
}

class _LatMarkerState extends State<LatMarker> {
  final Set<Marker> _markers = {};
  final LatLng _currentPosition = LatLng(-7.615697, 110.939672);
  @override
  void initState() {
    _markers.add(
      Marker(
        markerId: MarkerId("-7.615697, 110.939672"),
        position: _currentPosition,
        icon: BitmapDescriptor.defaultMarker,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Maps Flutter'),
      ),
      body: Stack(children: <Widget>[
        GoogleMap(
          onTap: (position) {
            setState(() {
              _markers.add(
                Marker(
                  markerId: MarkerId("${position.latitude}, ${position.longitude}"),
                  icon: BitmapDescriptor.defaultMarker,
                  position: position,
                ),
              );
            });
          },
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: _currentPosition,
            zoom: 14.0,
          ),
          markers: _markers,
        ),
      ]),
    );
  }
}
