import 'dart:convert';
import 'dart:io';

import 'Mahasiswa.dart';
import 'package:http/http.dart' as http;

class HttpClientService {
  static String rootUrl = "https://192.168.1.103:45455/";

  static Future<HttpClientRequest> postHttpClientRequest(String url) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    return request;
  }

  static Future<HttpClientRequest> getHttpClientRequest(String url) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client.getUrl((Uri.parse(url)));
    return request;
  }

  static Future<Mahasiswa> getStudent(int id) async {
    String url = rootUrl + 'Mahasiswa/GetStudent';
    HttpClientRequest request = await postHttpClientRequest(url);

    Mahasiswa mahasiswa = new Mahasiswa(Id: id, Nama: "", Age: '');
    request.add(utf8.encode(jsonEncode(mahasiswa.toJson())));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();

    if (response.statusCode == 200) {
      return Mahasiswa.fromJson(jsonDecode(reply));
    } else {
      throw Exception('Failed to Get Mahasiswa.');
    }
  }

  static Future<List<Mahasiswa>> getStudents() async {
    String url = rootUrl + 'Mahasiswa/GetMahasiswas';
    HttpClientRequest request = await getHttpClientRequest(url);

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();

    if (response.statusCode == 200) {
      var data = jsonDecode(reply);
      Iterable l = json.decode(reply);
      List<Mahasiswa> mhss =
          List<Mahasiswa>.from(l.map((model) => Mahasiswa.fromJson(model)));
      return mhss;
    } else {
      throw Exception('Failed to Get Mahasiswa.');
    }
  }

  static Future<String> delStudent(int id) async {
    String url = rootUrl + 'Mahasiswa/DeleteStudent';
    HttpClientRequest request = await postHttpClientRequest(url);

    Mahasiswa mahasiswa = new Mahasiswa(Id: id, Nama: "", Age: '');
    request.add(utf8.encode(jsonEncode(mahasiswa.toJson())));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();

    if (response.statusCode == 200) {
      return reply;
    } else {
      throw Exception('Failed to Delete Mahasiswa.');
    }
  }

  static Future<String> updateStudent(Mahasiswa mahasiswa) async {
    String url = rootUrl + 'Mahasiswa/UpdateStudent';
    HttpClientRequest request = await postHttpClientRequest(url);

    request.add(utf8.encode(jsonEncode(mahasiswa.toJson())));

    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();

    if (response.statusCode == 200) {
      return reply;
    } else {
      throw Exception('Failed to Update Mahasiswa.');
    }
  }
}
